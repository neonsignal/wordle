#ifdef _SCRIPT
filename=$0; set -x; g++ -std=c++20 -O2 -o ${filename%%.*} ${filename}; exit
#endif
// evaluate wordle strategy
// Copyright 2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#define ANSWERS // limit words to answer subset
//#define HARD // limit guesses to strictly possible answers
#define CACHE // cache results to improve speed
#include "crc.h"
#include <array>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <iostream>
//namespace std {template<typename T> int ssize(T const &x) {return x.size();}};
using std::array;
using std::vector;
using std::ssize;
using std::sort;
using std::pair;
using std::string;
using std::map, std::unordered_map;

class Wordle
{
	// clues for each guess for each answer
	vector<vector<int>> clues;

	// list of answer indices and lookup in guess list
	vector<int> guesses_lookup;

	// pruning
	const int prune{0};

	// maximum number of guesses
	const int depth_max{6};

	// limit of total guesses across all possible answers
	const int cost_limit{0};

	// cache of best results
	#ifdef CACHE
	unordered_map<uint64_t, int> costs_cache;
	#endif
public:
	// set up precalculations
	// answer_words: list of possible answers
	// guess_words: list of possible guesses
	// cost_limit: limit of total guesses across all possible answers
	// prune: smaller numbers sacrifice accuracy for speed, 0 is no pruning
	// depth_max: maximum number of guesses
	Wordle(const vector<string> &answer_words, const vector<string> &guess_words, int prune = 0, int depth_max = 6)
	: prune(prune), depth_max(depth_max), cost_limit(depth_max*ssize(answer_words))
	{
		// calculate all possible clues
		for (int i = 0; i < ssize(guess_words); ++i)
		{
			clues.emplace_back(vector<int>{});
			for (int j = 0; j < ssize(answer_words); ++j)
			{
				clues[i].emplace_back(0);
				string answer{answer_words[j]}, guess{guess_words[i]};
				int clue{};
				bool marked[5]{};

				// flag direct matches
				for (int c = 0; c < 5; ++c)
					if (guess[c] == answer[c])
						marked[c] = true;

				// for each letter of guess
				for (int c = 0; c < 5; ++c)
				{
					int match{c};

					// if not an exact match
					if (guess[c] != answer[c])

						// look through all the letters of the guess
						for (match = 0; match < 5; ++match)

							// if matching and answer letter not already matched and not an exact match
							if (!marked[match] && guess[c] == answer[match])
								break;

					// don't match answer letter twice
					marked[match] = true;

					// accumulate clues
					clue *= 3;
					clue += match == c ? 2 : match < 5 ? 1 : 0;
				}
				clues[i][j] = clue;
			}
		}

		// create answer lookup table
		for (int answer = 0; answer < ssize(answer_words); ++answer)
			for (int guess = 0; guess < ssize(guess_words); ++guess)
				if (answer_words[answer] == guess_words[guess])
					guesses_lookup.emplace_back(guess);
	}

	// display the log of clues and words
	// log: log found during the evaluation of a word
	// answer_words: list of words
	void display_log(const vector<string> &guess_words, const vector<int> &log)
	{
		int i{0};
		std::cout << guess_words[log[i++]] << std::endl;
		while (i < ssize(log))
		{
			int clue = log[i++];
			constexpr int n_clues{3*3*3*3*3};
			int depth = clue / n_clues;
			std::cout << string(depth, '\t') << depth;
			clue %= n_clues;
			for (int k = 0; k < 5; ++k)
			{
				constexpr char colors[] = {'-', 'Y', 'G'};
				char color = colors[clue/(3*3*3*3)];
				clue = clue%(3*3*3*3)*3;
				std::cout << color;
			}
			std::cout << ':' << guess_words[log[i++]] << std::endl;
		}
	}

	// evaluate cost of a single guess
	// guess: guess being evaluated
	// p_log: log of optimal guesses
	// [return]: minimum total cost across all answers if less than limit, zero otherwise
	int evaluate(int guess, vector<int> *p_log = 0)
	{
		// create list of answers
		vector<int> answers;
		for (int answer = 0; answer < ssize(guesses_lookup); ++answer)
			answers.emplace_back(answer);

		// cost of first guess
		int cost = ssize(answers);

		// minumum cost of second guess
		// assume possible direct match
		cost += ssize(answers)-1;

		// count partitions
		constexpr int n_clues{3*3*3*3*3};
		array<int, n_clues> counts{};
		int partitions{0};
		for (int answer: answers)
			if (++counts[clues[guess][answer]] == 1)
				++partitions;

		// adjust if not a direct match
		// as previous minimum cost assumed this
		cost += 1-counts[n_clues-1];

		// minimum cost of following guess
		// assume possible direct match in each partition
		cost += ssize(answers)-partitions;

		// check if already exceeded limit
		if (cost >= cost_limit)
			return cost_limit;

		// calculate remainder
		cost += calculate_guess(answers, cost_limit-cost, guess, 1, p_log);
		if (cost >= cost_limit)
			cost = 0;
		return cost;
	}

	// evaluate cost of any guess
	// p_log: log of optimal guesses
	// [return]: minimum total cost across all answers if less than limit, zero otherwise
	int evaluate(vector<int> *p_log = 0)
	{
		// create list of answers
		vector<int> answers;
		for (int answer = 0; answer < ssize(guesses_lookup); ++answer)
			answers.emplace_back(answer);

		// minumum cost of first guess
		// assume possible direct match
		int cost = ssize(answers)-1;

		// calculate remainder
		cost += calculate_list(answers, cost_limit-cost, 0, p_log);
		if (cost >= cost_limit)
			cost = 0;
		return cost;
	}

private:
	// calculate cost of guess
	// answers: list of answers being considered
	// cost_limit: limit on cost above estimated minimum
	// guess: guess being evaluated
	// depth: guess index
	// p_log: log of optimal guesses
	// [return]: minimum extra cost across all answers if less than limit
	int calculate_guess(const vector<int> &answers, int cost_limit, int guess, int depth, vector<int> *p_log = 0)
	{
		// log guess
		if (p_log)
			p_log->emplace_back(guess);

		// partition answers by clue
		constexpr int n_clues{3*3*3*3*3};
		array<vector<int>, n_clues> partitions{};
		for (int answer: answers)
			partitions[clues[guess][answer]].emplace_back(answer);

		// remove direct match
		partitions.back().clear();

		// sort largest first
		sort(partitions.begin(), partitions.end(), [](const vector<int> &a, const vector<int> &b) {return ssize(a) > ssize(b);});

		// for each sublist
		int cost{0};
		for (auto answers: partitions)
		{
			if (ssize(answers) == 0)
				break;

			// log clue
			if (p_log)
				p_log->emplace_back(depth*n_clues + clues[guess][answers[0]]);

			// recursively evaluate cost
			if (ssize(answers) <= 2)
				calculate_guess(answers, 1, guesses_lookup[answers[0]], depth+1, p_log);
			else if ((cost += calculate_list(answers, cost_limit-cost, depth, p_log)) >= cost_limit)
				break;
		}

		return cost;
	}

	// calculate cost of list
	// answers: list of answers being considered
	// cost_limit: limit on cost above estimated minimum
	// depth: guess index
	// [return]: minimum total cost across all answers if less than limit
	int calculate_list(const vector<int> &answers, int cost_limit, int depth, vector<int> *p_log)
	{
		// check cache
		#ifdef CACHE
		Crc64_ECMA182 crc;
		crc += depth;
		for (auto answer: answers)
		{
			crc += answer;
			crc += answer>>8;
		}
		auto cost_cache{costs_cache.find(crc)};
		if (cost_cache != costs_cache.end())
			return cost_cache->second;
		#endif

		// re-order selection
		vector<pair<int,int>> counts_min;
		#ifdef HARD
		for (int i = 0; i < ssize(answers); ++i)
		{
			int guess = guesses_lookup[answers[i]];
		#else
		for (int guess = 0; guess < ssize(clues); ++guess)
		{
		#endif
			int cost{0};

			// count partitions
			constexpr int n_clues{3*3*3*3*3};
			array<int, n_clues> counts{};
			int partitions{0};
			for (int answer: answers)
				if (++counts[clues[guess][answer]] == 1)
					++partitions;

			// adjust if not a direct match
			// as previous minimum cost assumed this
			cost += 1-counts[n_clues-1];

			// ignore guess if no advance or not possible to complete in time
			if (partitions == 1 || (ssize(answers) > partitions && depth+2 == depth_max))
				continue;

			// minimum cost of following guess
			// assume possible direct match in each partition
			cost += ssize(answers)-partitions;

			// build list of possible guesses
			if (cost < cost_limit)
				counts_min.emplace_back(pair(cost, guess));
		}
		sort(counts_min.begin(), counts_min.end());

		// prune list for speed vs accuracy
		if (prune)
			if (counts_min.size() > prune)
				counts_min.resize(prune);

		// find best next guess
		int cost_base{0};
		int guess_best{-1};
		for (auto count_min: counts_min)
		{
			if (count_min.first >= cost_limit)
				break;
			int cost = count_min.first+calculate_guess(answers, cost_limit-count_min.first, count_min.second, depth+1);
			if (cost < cost_limit)
			{
				cost_base = count_min.first;
				guess_best = count_min.second;
				cost_limit = cost;
			}
		}

		// log and cache result
		if (p_log && guess_best >= 0)
		{
			calculate_guess(answers, cost_limit-cost_base+1, guess_best, depth+1, p_log);
			#ifdef CACHE
			costs_cache[crc] = cost_limit;
			#endif
		}

		return cost_limit;
	}
};

#include <iostream>
using std::cin, std::cout, std::cerr, std::endl;
int main()
{
	// read in words
	vector<string> answer_words, guess_words;
	bool extra{false};
	do
	{
		string s;
		cin >> s;
	if (!cin) break;
		if (s == "/*EXTRA*/")
		{
			extra = true;
			continue;
		}
		if (ssize(s) != 5)
			throw("incorrect word length");
		#ifdef ANSWERS
		if (!extra)
		{
			answer_words.emplace_back(s);
			guess_words.emplace_back(s);
		}
		#else
		if (!extra)
			answer_words.emplace_back(s);
		else
			guess_words.emplace_back(s);
		#endif
	}
	while (true);

	// pre-calculate clue matches
	Wordle wordle(answer_words, guess_words);

	// for each word
	for (int i = 0; i < ssize(guess_words); ++i)
	{
		cerr << guess_words[i] << ' ';
		cerr.flush();
		vector<int> log;
		int cost = wordle.evaluate(i, &log);
		if (cost)
			cerr << cost;
		cerr << endl;
		if (cost)
			wordle.display_log(guess_words, log);
	}
}
