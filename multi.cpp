#ifdef _SCRIPT
filename=$0; set -x; g++ -std=c++20 -O2 -o ${filename%%.*} ${filename}; exit
#endif
// evaluate wordle strategy
// Copyright 2022 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#define ANSWERS // limit words to answer subset
//#define HARD // limit guesses to strictly possible answers
#include <array>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <cmath>
#include <iostream>
//namespace std {template<typename T> int ssize(T const &x) {return x.size();}};
using std::array;
using std::vector;
using std::ssize;
using std::sort;
using std::pair;
using std::string;
using std::map, std::unordered_map;

class Wordle
{
	// clues for each guess for each answer
	vector<vector<int>> clues;

	// list of answer indices and lookup in guess list
	vector<int> guesses_lookup;

	// limit of total guesses across all possible answers
	const int cost_limit{0};

	// maximum number of guesses
	const int depth_max{6};
public:
	// set up precalculations
	// answer_words: list of possible answers
	// guess_words: list of possible guesses
	// cost_limit: limit of total guesses across all possible answers
	// depth_max: maximum number of guesses
	Wordle(const vector<string> &answer_words, const vector<string> &guess_words, int cost_limit, int depth_max = 6)
	: cost_limit(cost_limit), depth_max(depth_max)
	{
		// calculate all possible clues
		for (int i = 0; i < ssize(guess_words); ++i)
		{
			clues.emplace_back(vector<int>{});
			for (int j = 0; j < ssize(answer_words); ++j)
			{
				clues[i].emplace_back(0);
				string answer{answer_words[j]}, guess{guess_words[i]};
				int clue{};
				bool marked[5]{};

				// flag direct matches
				for (int c = 0; c < 5; ++c)
					if (guess[c] == answer[c])
						marked[c] = true;

				// for each letter of guess
				for (int c = 0; c < 5; ++c)
				{
					int match{c};

					// if not an exact match
					if (guess[c] != answer[c])

						// look through all the letters of the guess
						for (match = 0; match < 5; ++match)

							// if matching and answer letter not already matched and not an exact match
							if (!marked[match] && guess[c] == answer[match])
								break;

					// don't match answer letter twice
					marked[match] = true;

					// accumulate clues
					clue *= 3;
					clue += match == c ? 2 : match < 5 ? 1 : 0;
				}
				clues[i][j] = clue;
			}
		}

		// create answer lookup table
		for (int answer = 0; answer < ssize(answer_words); ++answer)
			for (int guess = 0; guess < ssize(guess_words); ++guess)
				if (answer_words[answer] == guess_words[guess])
					guesses_lookup.emplace_back(guess);

		// test word combinations
		vector<vector<int>> guesses_list{}, guesses_list1;
		//guesses_list.emplace_back(vector<int>{});
		static int test_words[] = {2116, 1965, 1531, 2035, 1922, 1421, 2152, 1898, 2001, 2147, 2095, 1937, 1844, 2084, 2046, 1924, 1780, 1138, 2122, 943, 1759, 2031, 1145, 1788, 2301, 1894, 1839, 2026, 1803, 2145, 1534, 2197, 1117, 1767, 1895, 1681, 1777, 695, 1683, 1649, 1976, 1623, 1606, 1846, 1876, 1660, 2052, 2013, 1903, 1858, 1779, 1551, 1766, 1817, 1867, 1735, 1659, 1843, 2237, 1884, 1296, 1829, 1856, 1710, 1028, 2045, 1694, 1917, 1888, 1609, 1418, 1878, 1914, 1905, 1124, 1751, 1699, 1996, 1301, 1690, 1163, 1565, 1180, 1622, 1229, 2269, 1018, 1435, 1259, 2070, 1540, 1761, 1276, 1083, 1811, 1169, 687, 2078, 1251, 1897, 1078, 1791, 1486, 1675, 1849, 1524, 1438, 1190, 1666, 1866, 1291, 1768, 1153, 762, 776, 1424, 2144, 1655, 1502, 1848, 1477, 1617, 1130, 1509, 1701, 467, 1676, 1757,};
		for  (int i = 0; i < 128; ++i)
			for (int j = i+1; j < 128; ++j)
				guesses_list.emplace_back(vector<int>{test_words[i]-1, test_words[j]-1});
		int prune = 4;
		constexpr int levels{5};
		for (int k = 2; k < levels; ++k)
		{
			for (auto guesses: guesses_list)
			{
				vector<pair<float, int>> entropies{};
				#ifdef ANSWERS
				for (int i = 0; i < ssize(guesses_lookup); ++i)
				{
					int guess = guesses_lookup[i];
				#else
				for (int guess = 0; guess < ssize(clues); ++guess)
				{
				#endif

					// count partitions
					unordered_map<int64_t, int> counts{};
					for (int answer = 0; answer < ssize(guesses_lookup); ++answer)
					{
						constexpr int n{256}; // >= 3^5
						int64_t hash{};
						for (int guess: guesses)
							hash = hash*n + clues[guess][answer];
						hash = hash*n + clues[guess][answer];
						++counts[hash];
					}

					// calculate entropy
					double entropy{0};
					for (auto count: counts)
						entropy += count.second*log2(count.second);
					/*
					int unique{0};
					for (auto count: counts)
						unique += count.second == 1;
					*/

					entropies.emplace_back(pair{entropy, guess});
				}
				sort(entropies.begin(), entropies.end());
				entropies.resize(prune);
				for (auto entropy: entropies)
				{
					guesses_list1.emplace_back(guesses);
					guesses_list1.back().emplace_back(entropy.second);
					std::cout << entropy.first << ' ';
					for (int i = 0; i < k; ++i)
						std::cout << guess_words[guesses[i]] << ' ';
					std::cout << guess_words[entropy.second] << std::endl;
				}
			}
			guesses_list = guesses_list1;
			guesses_list1.resize(0);
			prune /= 2;
		}
	}
};

#include <iostream>
#include <iomanip>
using std::cin, std::cout, std::cerr, std::endl;
int main()
{
	// read in words
	vector<string> answer_words, guess_words;
	bool extra{false};
	do
	{
		string s;
		cin >> s;
	if (!cin) break;
		if (s == "/*EXTRA*/")
		{
			extra = true;
			continue;
		}
		if (ssize(s) != 5)
			throw("incorrect word length");
		#ifdef ANSWERS
		if (!extra)
		{
			answer_words.emplace_back(s);
			guess_words.emplace_back(s);
		}
		#else
		if (!extra)
			answer_words.emplace_back(s);
		else
			guess_words.emplace_back(s);
		#endif
	}
	while (true);

	// pre-calculate clue matches
	Wordle wordle(answer_words, guess_words, 13891);
}
